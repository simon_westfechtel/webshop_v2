-- 1.
SELECT last_name FROM customer WHERE last_name REGEXP '^A|^B|^C';

-- 2.
SELECT CONCAT (first_name,' ',last_name) AS Name FROM customer WHERE active=1;

-- 3.
SELECT customer_id,email FROM customer ORDER BY LENGTH (email) LIMIT 10;

-- 4.
SELECT store_id, COUNT(customer_id) AS `Anzahl der Kunden` FROM customer GROUP BY store_id;

-- 5.
SET @children := (SELECT category_id FROM category WHERE name='Children');
SELECT film_id,title,rental_rate FROM film WHERE film_id IN (SELECT film_id FROM film_category WHERE category_id=@children) AND rental_rate<1 AND rating='G';
-- oder ist einfach nur das gemeint? SELECT film_id,title,rental_rate FROM film WHERE rental_rate<1 AND rating='G';

-- 6.
SELECT film_id, COUNT(film_id) AS Anzahl FROM inventory WHERE store_id=1 GROUP BY film_id;
-- eventuell auf film ausfuehren?

-- 7.
SELECT inventory.store_id, inventory.film_id, COUNT(inventory.film_id) AS 'Anzahl Kopien', film.rating 
FROM inventory JOIN film ON inventory.film_id = film.film_id
GROUP BY inventory.film_id, inventory.store_id
ORDER BY inventory.store_id, film.rating, inventory.film_id;

-- 8.
SET @children := (SELECT category_id FROM category WHERE name='Children');
SELECT film_id, rating, title from film WHERE film_id IN (SELECT film_id FROM film_category WHERE category_id=@children) AND rating = 'R' OR rating = 'NC-17';

-- 9.
SET @germany := (SELECT country_id FROM country WHERE country = 'Germany');

SELECT customer.customer_id, customer.first_name, customer.last_name, customer.email, address.address, address.postal_code, city.city 
FROM customer JOIN address ON customer.address_id = address.address_id JOIN city ON address.city_id = city.city_id
WHERE address.city_id IN (SELECT city_id FROM city WHERE country_id = @germany);  

-- 10.
SELECT country, COUNT(customer_id) AS Anzahl
FROM customer JOIN address ON customer.address_id = address.address_id
JOIN city ON address.city_id = city.city_id
JOIN country ON city.country_id = country.country_id
GROUP BY country 
HAVING Anzahl >= 30
ORDER BY Anzahl DESC;

-- 11.
SELECT first_name, last_name, MONTHNAME(payment_date) AS Monat, amount FROM payment JOIN customer ON payment.customer_id = customer.customer_id
WHERE payment_date >= '2005-01-01 00:00:00' AND payment_date < '2005-07-01 00:00:00'
GROUP BY payment.customer_id
ORDER BY Monat DESC, amount DESC;

-- 12.
SELECT MONTHNAME(payment_date) AS Monat, SUM(amount) AS Umsatz FROM payment GROUP BY Monat ORDER BY Umsatz DESC LIMIT 1;

-- 13.
SELECT category.name AS Kategorie, inventory.store_id AS Store, SUM(payment.amount) AS Umsatz FROM payment
JOIN rental ON payment.rental_id = rental.rental_id
JOIN inventory ON rental.inventory_id = inventory.inventory_id
JOIN store ON inventory.store_id = store.store_id
-- JOIN film ON inventory.film_id = film.film_id
JOIN film_category ON inventory.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
GROUP BY Kategorie, Store;

-- 14.
WITH RECURSIVE supervisors AS 
 ( SELECT * FROM staff
   WHERE staff_id = 6
   UNION
   SELECT s.*
   FROM staff AS s, supervisors AS sup
   WHERE s.staff_id = sup.supervisor_id )
SELECT first_name, last_name FROM supervisors ORDER BY supervisor_id;

-- 15.
SHOW COLUMNS FROM sakila.staff WHERE `Null` LIKE 'YES';