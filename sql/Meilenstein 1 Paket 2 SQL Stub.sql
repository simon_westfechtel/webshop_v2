-- SQL stub, nutzen Sie dieses Snippet als Ausgangspunkt und erweitern und ändern Sie es so, 
-- dass es die Aufgaben aus Paket 2 löst

--
--
-- DROP statements
-- zuvor angelegte Tabellen löschen, niemals die ganze Datenbank löschen, nur Tabellen
-- mit DROP werden sowohl die Schemadefinitionen der Tabelle als auch die in ihr gespeicherten Daten


DROP TABLE IF EXISTS `Mitarbeiter`;
DROP TABLE IF EXISTS `Student`;
DROP TABLE IF EXISTS `Gast`;
DROP TABLE IF EXISTS `FH_Angehörige`;
DROP TABLE IF EXISTS `FE_Nutzer`;

DROP TABLE IF EXISTS `B_enthält_P`;
DROP TABLE IF EXISTS `Bestellung`;

DROP TABLE IF EXISTS `P_enthält_Z`;
DROP TABLE IF EXISTS `Zutat`;

DROP TABLE IF EXISTS `Preis`;
DROP TABLE IF EXISTS `Bild`;
DROP TABLE IF EXISTS `Kategorie`;
DROP TABLE IF EXISTS `Produkt`;

DROP TABLE IF EXISTS `Auth_type`;
DROP TABLE IF EXISTS `Name_type`;
--
--
-- CREATE statements
-- Schemaelemente definieren

CREATE TABLE `Kategorie` (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	Bezeichnung VARCHAR(100) NOT NULL DEFAULT '',
	Oberkategorie MEDIUMINT DEFAULT NULL, 	-- DEFAULT NULL ist standard
	-- Produkt MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_OberKategorie_kat` FOREIGN KEY (Oberkategorie) REFERENCES Kategorie(ID) ON DELETE CASCADE,
	-- CONSTRAINT `fk_Produkt_kat` FOREIGN KEY (Produkt) REFERENCES Produkt(ID),
	PRIMARY KEY (ID)
);

CREATE TABLE `Produkt` (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	Beschreibung VARCHAR(255) NOT NULL DEFAULT '',
	Name VARCHAR(24) NOT NULL DEFAULT '',
	Kategorie MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Kategorie_prod` FOREIGN KEY (Kategorie) REFERENCES Kategorie(ID),
	PRIMARY KEY (ID)
);

CREATE TABLE `Preis` (
	Gastbetrag DECIMAL NOT NULL DEFAULT 0,
	Studentenbetrag DECIMAL NOT NULL DEFAULT 0,
	Mitarbeiterbetrag DECIMAL NOT NULL DEFAULT 0,
	Produkt MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Produkt_preis` FOREIGN KEY (Produkt) REFERENCES Produkt (ID),
	PRIMARY KEY (Gastbetrag,Studentenbetrag,Mitarbeiterbetrag)
);

-- Beispiel für benannten Foreign Key Constraint `OberKat`
-- FOREIGN KEY (Klammern!) REFERENCES Table(Column)


CREATE TABLE `Bild`(
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,	-- Unterschied MediumInt/Int
	Binaerdaten BLOB NOT NULL, 					-- Datentyp unbedingt verbessern
	AltText VARCHAR(60) NOT NULL DEFAULT '',					-- ist Attribut nicht optional ... 
	Title VARCHAR(60) NOT NULL DEFAULT '',						-- ... müssen Sie mit dem NOT NULL ...
	Unterschrift VARCHAR(80) DEFAULT NULL DEFAULT '',				-- ... Constraint arbeiten!
	Kategorie MEDIUMINT DEFAULT NULL,
	Produkt MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Kategorie_bild` FOREIGN KEY (Kategorie) REFERENCES Kategorie(ID),
	CONSTRAINT `fk_Produkt_bild` FOREIGN KEY (Produkt) REFERENCES Produkt(ID),
	PRIMARY KEY(ID)							-- Primarschlüssel der Tabelle festlegen
);

CREATE TABLE `Zutat` (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	Name VARCHAR(32) NOT NULL DEFAULT '',
	Beschreibung VARCHAR(255) DEFAULT NULL,
	Vegan BOOLEAN DEFAULT 0,
	Vegetarisch BOOLEAN DEFAULT 0,
	Bio BOOLEAN DEFAULT 0,
	Glutenfrei BOOLEAN DEFAULT 0,
	PRIMARY KEY (ID)
);

CREATE TABLE `Bestellung` (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE `B_enthält_P` (
	Anzahl MEDIUMINT NOT NULL,
	BID MEDIUMINT DEFAULT NULL,
	PID MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Bestellung_bep` FOREIGN KEY (BID) REFERENCES Bestellung(ID),
	CONSTRAINT `fk_Produkt_bep` FOREIGN KEY (PID) REFERENCES Produkt(ID)
);

CREATE TABLE `P_enthält_Z` (
	PID MEDIUMINT DEFAULT NULL,
	ZID MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Produkt_pez` FOREIGN KEY (PID) REFERENCES Produkt(ID),
	CONSTRAINT `fk_Zutat_pez` FOREIGN KEY (ZID) REFERENCES Zutat(ID),
	PRIMARY KEY (PID,ZID)
);

-- Warum soll hash 32 Zeichen lang sein wenn ein sha256 hash 64 Zeichen lang ist?
CREATE TABLE `Auth_type` (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	hash VARCHAR(64) NOT NULL DEFAULT '',
	salt VARCHAR(32) NOT NULL DEFAULT '',
	stretch MEDIUMINT NOT NULL DEFAULT 0,
	-- algo VARCHAR(6) NOT NULL,
	-- CHECK (algo='sha1' OR algo='sha256'),
	algo ENUM('sha1', 'sha256'),
	PRIMARY KEY (ID)
);


CREATE TABLE Name_type (
	ID MEDIUMINT AUTO_INCREMENT NOT NULL,
	Vorname VARCHAR(24) NOT NULL DEFAULT '',
	Nachname VARCHAR(24) NOT NULL DEFAULT '',
	PRIMARY KEY (ID)
);

-- FE-Nutzer: Frage nach Realisierung strukturierter Attribute
-- Da MySQL kein CREATE TYPE bietet fallen folgende Möglichkeiten ein:
-- a) Für strukturierte Attribute neue Tabelle anlegen und in einer Spalte referenzieren?
-- b) Struktur ignorieren und einzelne Attribute als Spalten anlegen?
CREATE TABLE `FE_Nutzer` (
	NR MEDIUMINT AUTO_INCREMENT NOT NULL,
	Auth_id MEDIUMINT NOT NULL,
	-- Alternativ:
	-- hash VARCHAR(64) NOT NULL DEFAULT '',
	-- salt VARCHAR(32) NOT NULL DEFAULT '',
	-- stretch MEDIUMINT NOT NULL DEFAULT 0,
	-- algo VARCHAR(6) NOT NULL,
	-- CHECK (algo='sha1' OR algo='sha256'),
	-- algo ENUM('sha1', 'sha256'),
	Loginname VARCHAR(24) NOT NULL DEFAULT '',
	Email VARCHAR(32) NOT NULL,
	CONSTRAINT `emailEindeutig` UNIQUE (Email),
	Aktiv BOOLEAN NOT NULL DEFAULT 0,
	Anlegedatum DATE NOT NULL DEFAULT '2017-01-01',
	Name_id MEDIUMINT NOT NULL,
	-- Alternativ:
	-- Vorname VARCHAR(24) NOT NULL DEFAULT '',
	-- Nachname VARCHAR(24) NOT NULL DEFAULT '',
	LetzerLogin DATE NOT NULL DEFAULT '2017-01-01',
	Bestellung MEDIUMINT DEFAULT NULL,
	CONSTRAINT `fk_Bestellung_fen` FOREIGN KEY (Bestellung) REFERENCES Bestellung(ID),
	CONSTRAINT `fk_Name_type_fen` FOREIGN KEY (Name_id) REFERENCES Name_type(ID), 
	CONSTRAINT `fk_Auth_type_fen` FOREIGN KEY (Auth_id) REFERENCES Auth_type(ID),
	PRIMARY KEY (NR)
);

CREATE TABLE `Gast` (
	NR MEDIUMINT NOT NULL,
	Grund VARCHAR(32) NOT NULL DEFAULT '',
	Ablauf DATE NOT NULL,
	CONSTRAINT `fk_FE_Nutzer_gast` FOREIGN KEY (NR) REFERENCES FE_Nutzer(NR) ON DELETE CASCADE,
	PRIMARY KEY (NR)
);

CREATE TABLE `FH_Angehörige` (
	NR MEDIUMINT NOT NULL,
	CONSTRAINT `fk_FE_Nutzer_fha` FOREIGN KEY (NR) REFERENCES FE_Nutzer(NR) ON DELETE CASCADE,
	PRIMARY KEY (NR)
);

CREATE TABLE `Mitarbeiter` (
	NR MEDIUMINT NOT NULL,
	MA_Nummer INT NOT NULL DEFAULT 0,
	Telefonnummer VARCHAR(24) DEFAULT NULL,
	Büro VARCHAR(12) DEFAULT NULL,
	CONSTRAINT `fk_FH_Angehörige_ma` FOREIGN KEY (NR) REFERENCES FH_Angehörige(NR) ON DELETE CASCADE,
	PRIMARY KEY (NR)
);

CREATE TABLE `Student` (
	NR MEDIUMINT NOT NULL,
	Matrikelnummer INT NOT NULL,
	CONSTRAINT `Matrikelunique` UNIQUE (Matrikelnummer),
	CONSTRAINT `Matrikelrange` CHECK (Matrikelnummer >= 10000 AND Matrikelnummer <= 9999999),
	Studiengang VARCHAR(24) NOT NULL DEFAULT '',
	CONSTRAINT `fk_FH_Angehörige_st` FOREIGN KEY (NR) REFERENCES FH_Angehörige(NR) ON DELETE CASCADE,
	PRIMARY KEY (NR)
);

CREATE TRIGGER `currentdate` BEFORE INSERT ON  `Gast` 
FOR EACH ROW 
SET NEW.Ablauf = CURDATE();

--
--
-- INSERT statements
-- wenn alle Tabellen vollständig definiert sind, fügen Sie Beispieldaten in die Benutzertabellen ein (Aufgabe 2.4)
-- hier ein Insert-Beispiel für die im Stub definierte Tabelle Kategorie (wenn Sie das Schema ändern, kann es auch dazu kommen, dass Sie Änderungen in den INSERT Statements vornehmen müssen)

-- INSERT INTO `Kategorie` (ID, Bezeichnung, Oberkategorie) VALUES (1, 'Gebäck', NULL);

-- da ID automatisch vergeben wird, und die Oberkategorie per Default NULL ist, muss pro Eintrag nur das Attribut "Bezeichnung" angegeben werden
-- ausserdem können Sie in einem INSERT Statement mehrere Entitäten zum speichern angeben
-- INSERT INTO `Kategorie` (Bezeichnung) VALUES ('Vorspeisen'),('Desserts'),('Snacks'),('Mittagessen');

-- Generell 

INSERT INTO Auth_type (hash, salt, stretch, algo) VALUES ('hash1', 'salt1', 1, 'sha256');
INSERT INTO Name_type (Vorname, Nachname) VALUES ('Simon', 'Westfechtel');

SET @v1 := (SELECT ID FROM Auth_type WHERE hash = 'hash1');
SET @v2 := (SELECT ID FROM Name_type WHERE Nachname = 'Westfechtel');

INSERT INTO FE_Nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, Name_id, LetzerLogin, Bestellung) 
						VALUES (@v1, 'swest', 'simon.westfechtel@gmail.com', 0, '2017-11-16', @v2, '2017-11-16', NULL);


-- Mitarbeiter

INSERT INTO Auth_type (hash, salt, stretch, algo) VALUES ('hash2', 'salt2', 1, 'sha256');
INSERT INTO Name_type (Vorname, Nachname) VALUES ('Hokus', 'Pokus');

SET @v1 := (SELECT ID FROM Auth_type WHERE hash = 'hash2');
SET @v2 := (SELECT ID FROM Name_type WHERE Nachname = 'Pokus');

INSERT INTO FE_Nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, Name_id, LetzerLogin, Bestellung) 
						VALUES (@v1, 'b2', 'mail2', 0, '2017-11-16', @v2, '2017-11-16', NULL);
SET @v1 := (SELECT NR FROM FE_Nutzer WHERE Loginname = 'b2');
INSERT INTO FH_Angehörige (NR) VALUES (@v1);
INSERT INTO Mitarbeiter (NR, MA_Nummer, Telefonnummer, Büro) VALUES (@v1, 4711, '5555555555', 'Raum001');

-- Studenten

INSERT INTO Auth_type (hash, salt, stretch, algo) VALUES ('hash3', 'salt3', 1, 'sha256');
INSERT INTO Name_type (Vorname, Nachname) VALUES ('Max', 'Mustermann');

SET @v1 := (SELECT ID FROM Auth_type WHERE hash = 'hash3');
SET @v2 := (SELECT ID FROM Name_type WHERE Nachname = 'Mustermann');

INSERT INTO FE_Nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, Name_id, LetzerLogin, Bestellung) 
						VALUES (@v1, 'b3', 'mail3', 0, '2017-11-16', @v2, '2017-11-16', NULL);
SET @v1 := (SELECT NR FROM FE_Nutzer WHERE Loginname = 'b3');
INSERT INTO FH_Angehörige (NR) VALUES (@v1);
INSERT INTO Student (NR, Matrikelnummer, Studiengang) VALUES (@v1, 47120, 'BWL');



INSERT INTO Auth_type (hash, salt, stretch, algo) VALUES ('hash4', 'salt4', 1, 'sha256');
INSERT INTO Name_type (Vorname, Nachname) VALUES ('Marie', 'Musterfrau');

SET @v1 := (SELECT ID FROM Auth_type WHERE hash = 'hash4');
SET @v2 := (SELECT ID FROM Name_type WHERE Nachname = 'Musterfrau');

INSERT INTO FE_Nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, Name_id, LetzerLogin, Bestellung) 
						VALUES (@v1, 'b4', 'mail4', 0, '2017-11-16', @v2, '2017-11-16', NULL);
SET @v1 := (SELECT NR FROM FE_Nutzer WHERE Loginname = 'b4');
INSERT INTO FH_Angehörige (NR) VALUES (@v1);
INSERT INTO Student (NR, Matrikelnummer, Studiengang) VALUES (@v1, 47130, 'BWL');

-- Löschen

DELETE FROM `FE_Nutzer` WHERE NR = @v1;