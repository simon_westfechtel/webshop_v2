﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using MySql.Data.MySqlClient;

namespace webshop_v2.App_Code {
    public class SqlController {
        private MySqlConnection connection;

        public SqlController () {
            connection = new MySqlConnection ( ConfigurationManager.ConnectionStrings ["DbConStr"].ConnectionString );
            try {
                connection.Open ();
            }
            catch ( Exception e ) {
            }
        }

        public List < Produkt > GetProdukte () {
            List < Produkt > produkte = new List < Produkt > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Produkt";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                Produkt produkt = new Produkt ( reader.GetInt32 ( "ID" ) , reader.GetString ( "Beschreibung" ) ,
                    reader.GetString ( "Name" ) , reader.GetInt32 ( "Kategorie" ));
                produkte.Add ( produkt );
            }
            reader.Close ();
            return produkte;
        }

        public List < Bild > GetBilder () {
            List < Bild > bilder = new List < Bild > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Bild";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                //Bild bild = new Bild((int)reader["ID"], (string)reader["AltText"], (string)reader["Title"], (string)reader["Unterschrift"], Convert.ToBase64String((byte[])reader["Binärdaten"]));
                Bild bild = new Bild (
                    reader.GetInt32 ( "ID" ) , reader.GetString ( "AltText" ) , reader.GetString ( "Title" ) ,
                    reader.GetString ( "Unterschrift" ) ,
                    Convert.ToBase64String ( ( byte [ ] ) reader ["Binaerdaten"] ) ,
                    reader.GetInt32 ( "Kategorie" ) , reader.GetInt32 ( "Produkt" ) );
                bilder.Add ( bild );
            }
            reader.Close ();
            return bilder;
        }

        public List < Kategorie > GetOberkategorien () {
            List < Kategorie > kategorien = new List < Kategorie > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Kategorie WHERE Oberkategorie = ID";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                Kategorie kategorie = new Kategorie ( reader.GetInt32 ( "ID" ) , reader.GetString ( "Bezeichnung" ) ,
                    reader.GetInt32 ( "Oberkategorie" ));
                kategorien.Add ( kategorie );
            }
            reader.Close ();
            return kategorien;
        }

        public List<Kategorie> GetUnterkategorien()
        {
            List<Kategorie> kategorien = new List<Kategorie>();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Kategorie WHERE Oberkategorie != ID";
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                Kategorie kategorie = new Kategorie(reader.GetInt32("ID"), reader.GetString("Bezeichnung"),
                    reader.GetInt32("Oberkategorie"));
                kategorien.Add(kategorie);
            }
            reader.Close();
            return kategorien;
        }

        public List < int > GetStudenten () {
            List <int> studenten = new List<int>();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT NR FROM student WHERE 1";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                studenten.Add ( reader.GetInt32 ("NR") );
            }
            reader.Close ();
            return studenten;
        }

        public List<int> GetMitarbeiter()
        {
            List<int> mitarbeiter = new List<int>();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT NR FROM mitarbeiter WHERE 1";
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                mitarbeiter.Add(reader.GetInt32("NR"));
            }
            reader.Close();
            return mitarbeiter;
        }

        public List<int> GetGaeste()
        {
            List<int> gaeste = new List<int>();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT NR FROM gast WHERE 1";
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                gaeste.Add(reader.GetInt32("NR"));
            }
            reader.Close();
            return gaeste;
        }

        public User LoginUser ( string usr , string pass ) {
            var command = connection.CreateCommand ();
            string query = String.Format ( "SELECT Auth_id FROM FE_Nutzer WHERE Loginname = \'{0}\'", usr );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            if (!reader.HasRows) throw new BadLoginAttemptException ("Login fehlgeschlagen. Überprüfen Sie Ihren Loginnamen.");
            reader.Read ();
            int auth_id = reader.GetInt32 ( "Auth_id" );
            reader.Close ();

            query = String.Format("SELECT * FROM Auth_type WHERE ID = {0}", auth_id);
            command.CommandText = query;
            reader = command.ExecuteReader ();
            reader.Read ();
            string verify_hash = reader.GetString ( "hash" );
            reader.Close ();

            if ( PasswordSecurity.PasswordStorage.VerifyPassword ( pass , verify_hash ) ) {// password good
                List < int > studenten = GetStudenten ();
                List < int > mitarbeiter = GetMitarbeiter ();
                List < int > gaeste = GetGaeste ();
                string role = "Default-Nutzer";

                

                query = String.Format("SELECT * FROM FE_Nutzer WHERE Loginname = \'{0}\'", usr);
                command.CommandText = query;
                reader = command.ExecuteReader();
                reader.Read ();

                if (mitarbeiter.Contains(reader.GetInt32("NR"))) role = "Mitarbeiter";
                if (gaeste.Contains(reader.GetInt32("NR"))) role = "Gast";
                if (studenten.Contains(reader.GetInt32("NR"))) role = "Student";

                User user = new User (reader.GetInt32 ( "NR" ), reader.GetString ( "Loginname" ), reader.GetString ( "Email" ), 
                    reader.GetBoolean ("Aktiv"), reader.GetString ( "Anlegedatum" ), reader.GetString ( "Vorname" ), 
                    reader.GetString ( "Nachname" ), role);
                reader.Close ();
                return user;
            } else throw new BadLoginAttemptException ("Login fehlgeschlagen. Überprüfen Sie Ihr Passwort.");
        }

        public void RegisterUser ( string usr , string pass ) {
            var command = connection.CreateCommand ();
            string query = "";
            query = String.Format ( "INSERT INTO Auth_type (hash,salt,stretch,algo) VALUES (\'{0}\',\'\',0,\'sha256\')" ,
                PasswordSecurity.PasswordStorage.CreateHash ( pass ) );
            command.CommandText = query;
            command.ExecuteNonQuery ();

            query = String.Format("insert into fe_nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, LetzerLogin, Bestellung, Vorname, Nachname) " +
                                  "values (1, \'{0}\', \'mailsimonwest\', true, \'2017-12-08\', \'2017-12-08\', null, \'Simon\', \'Westfechtel\') ", usr);
            command.CommandText = query;
            command.ExecuteNonQuery();

            query = String.Format("insert into fh_angehörige (NR) values (1)");
            command.CommandText = query;
            command.ExecuteNonQuery();

            query = String.Format("INSERT INTO Student (NR, Matrikelnummer, Studiengang) VALUES (1, 353822, \'Informatik\')");
            command.CommandText = query;
            command.ExecuteNonQuery();
        }

        public Preis GetProduktPreis ( int produktId ) {
            var command = connection.CreateCommand();
            string query = "";
            query = String.Format ( "SELECT * FROM preis WHERE Produkt = {0}" , produktId );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            reader.Read ();
            Preis preis = new Preis(reader.GetDouble("Studentenbetrag"), reader.GetDouble ( "Gastbetrag" ), reader.GetDouble ( "Mitarbeiterbetrag" ), reader.GetInt32("Produkt"));
            reader.Close ();
            return preis;
        }
    }
}