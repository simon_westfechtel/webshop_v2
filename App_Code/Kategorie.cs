﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webshop_v2.App_Code {
    public class Kategorie {
        public Kategorie ( int id , string bezeichnung , int oberkategorie /*, int produkt*/ ) {
            this.Id = id;
            this.Bezeichnung = bezeichnung;
            this.Oberkategorie = oberkategorie;
            //this.Produkt = produkt;
        }

        public int Id { get; set; }
        public string Bezeichnung { get; set; }
        public int Oberkategorie { get; set; }
        //public int Produkt { get; set; }
    }
}