﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace webshop_v2.App_Code {
    public class Produkt {
        public Produkt ( int id , string beschreibung , string name , int kategorie ) {
            this.Id = id;
            this.Beschreibung = beschreibung;
            this.Name = name;
            this.Kategorie = kategorie;
        }

        public int Id { get; set; }
        public string Beschreibung { get; set; }
        public string Name { get; set; }
        public int Kategorie { get; set; }

        public bool Vegetarisch () {
            return false;
        }

        public bool Vegan () {
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Produkt objAsProdukt = obj as Produkt;
            if (objAsProdukt == null) return false;
            else return Equals(objAsProdukt);
        }
        public override int GetHashCode()
        {
            return Id;
        }
        public bool Equals(Produkt other)
        {
            if (other == null) return false;
            return (this.Id.Equals(other.Id));
        }
    }
}