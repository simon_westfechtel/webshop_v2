﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webshop_v2.App_Code {
    public class Bild {
        public Bild ( int id , string alttext , string titel , string unterschrift , string binaerdaten ,
            int kategorie , int produkt ) {
            this.Id = id;
            this.AltText = alttext;
            this.Titel = titel;
            this.Unterschrift = unterschrift;
            this.Binaerdaten = binaerdaten;
            this.Kategorie = kategorie;
            this.Produkt = produkt;
        }

        public int Id { get; set; }
        public string AltText { get; set; }
        public string Titel { get; set; }
        public string Unterschrift { get; set; }
        public string Binaerdaten { get; set; }
        public int Kategorie { get; set; }
        public int Produkt { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Bild objAsBild = obj as Bild;
            if (objAsBild == null) return false;
            else return Equals(objAsBild);
        }
        public override int GetHashCode()
        {
            return Id;
        }
        public bool Equals(Bild other)
        {
            if (other == null) return false;
            return (this.Id.Equals(other.Id));
        }
    }
}