﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace webshop_v2.App_Code {
    public class EventLogger {
        private StreamWriter streamWriter;

        public EventLogger ( string file ) {
            if ( File.Exists ( file ) )
                File.Delete ( file );
            streamWriter = File.CreateText ( file );
        }

        public void write ( string text ) {
            this.streamWriter.WriteLine ( text );
        }
    }
}