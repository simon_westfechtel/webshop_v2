﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.UI;

namespace webshop_v2.App_Code
{
    public class User
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public string Date { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Role { get; set; }

        public User (int id, string alias, string email, bool active, string date, string vorname, string nachname, string role) {
            this.Id = id;
            this.Alias = alias;
            this.Email = email;
            this.Active = active;
            this.Date = date;
            this.Vorname = vorname;
            this.Nachname = nachname;
            this.Role = role;
        }
    }
}