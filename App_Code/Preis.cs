﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webshop_v2.App_Code
{
    public class Preis
    {
        public double Studentenpreis { get; set; }
        public double Gastpreis { get; set; }
        public double Mitarbeiterpreis { get; set; }
        public double Produkt { get; set; }

        public Preis ( double studentenpreis , double gastpreis , double mitarbeiterpreis , double produkt ) {
            this.Studentenpreis = studentenpreis;
            this.Gastpreis = gastpreis;
            this.Mitarbeiterpreis = mitarbeiterpreis;
            this.Produkt = produkt;
        }
    }
}